package br.com.controllers;

import br.com.services.ContatoService;
import br.com.models.Contato;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
public class ContatoController {


    @Autowired
    ContatoService contatoService;

    @PostMapping("/contato")
    public void gravarContato(@RequestBody Contato contato, Principal principal){

        contato.setIdDonoContato(principal.getName());
        contatoService.salvarContato(contato);

    }

    @GetMapping("/contatos")
    public List<Contato> buscarContatos(Principal principal){

        return contatoService.buscarContatos(principal.getName());

    }

}
