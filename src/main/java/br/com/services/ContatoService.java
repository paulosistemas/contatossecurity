package br.com.services;

import br.com.models.Contato;
import br.com.repositories.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {

    @Autowired
    ContatoRepository contatoRepository;

    public Contato salvarContato(Contato contato){
        contatoRepository.save(contato);
        return contato;
    }

    public List<Contato> buscarContatos(String idDonoContato){

        return contatoRepository.findByIdDonoContato(idDonoContato);

    }

}
