package br.com.repositories;


import br.com.models.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {

    List<Contato> findByIdDonoContato(String idDonoContato);
}

//List<Acesso> findByIdSalaAndIdCliente(int idSala, int idCliente);